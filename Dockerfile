FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

ARG NUGET_USERNAME
ARG NUGET_TOKEN
ARG NUGET_PROJECT_ID=18697166

ENV NUGET_XMLDOC_MODE=none

WORKDIR /build

COPY src/ .
COPY ./build-docs.scl .
COPY ./build-step-toc.ps1 .

RUN dotnet nuget add source --name reductech --username $NUGET_USERNAME \
      --password $NUGET_TOKEN --store-password-in-clear-text \
      "https://gitlab.com/api/v4/projects/$NUGET_PROJECT_ID/packages/nuget/index.json"

RUN dotnet build -r linux-x64 -o ./bin/edr ./edr/EDR/EDR.csproj \
    && cp ./edr-appsettings.json ./bin/edr/appsettings.json \
    && cp ./connectors.json ./bin/edr/connectors.json \
    && dotnet ./bin/edr/edr.dll run ./build-docs.scl \
    && pwsh -File ./build-step-toc.ps1

FROM mono:6.12

ARG DOCFX_VERSION=v2.58.4

ENV NUGET_XMLDOC_MODE=none

WORKDIR /docs

RUN apt-get update && apt-get install -y unzip \
    && curl -L https://github.com/dotnet/docfx/releases/download/$DOCFX_VERSION/docfx.zip -o docfx.zip \
    && unzip docfx.zip -d docfx/

COPY . .
COPY --from=build /build/edr/steps/ ./edr/steps

CMD mono docfx/docfx.exe docfx.json --serve -n "*"

EXPOSE 8080
