# EDR Documentation

This project is built using [docfx](https://dotnet.github.io/docfx/).

## Building/testing locally

1. Install [docfx](https://dotnet.github.io/docfx/tutorial/docfx_getting_started.html#2-use-docfx-as-a-command-line-tool)
2. To build the EDR Steps documentation:

```powershell
dotnet publish -c Release -o ./bin/edr src/edr/EDR/EDR.csproj
cp ./src/edr-appsettings.json ./bin/edr/appsettings.json
cp ./src/connectors.json ./bin/edr/connectors.json
bin/edr/EDR run ./build-docs.scl
pwsh -File ./build-step-toc.ps1
```

3. For testing, run a local server:

```powershell
docfx.exe .\docfx.json --serve
```

4. To ensure links will work correctly when building on linux, build in Docker:

```powershell
PS > docker build --tag edr-docs-test --build-arg NUGET_USERNAME=<gitlab username> --build-arg NUGET_TOKEN=<personal access token> .
```

To run the container, use interactive mode for highlighting to work:

```powershell
docker run -it --rm -p 8080:8080 --name edr-docs edr-docs-test
```

A server will be available on http://localhost:8080/ and any broken links
will come up as a warning in the logs.

When the container is running, use `docker exec -it edr-docs bash` to browse files.

## Adding new connectors to the documentation

1. Add the submodule to the project. The paths used **must** be relative

```powershell
git submodule add ../connectors/sql.git src/connectors/sql
```

2. Add a summary file `edr/connectors/sql.md`
3. Add an entry to the connectors and examples TOC:

```yaml
- name: SQL
  href: sql.md
```

4. Add the connector to `index.md` (maintain alphabetical order)

5. Add the connector to `connectors.json`. (Use a prerelease version if necessary as this will be used to generate documentation)

## Updating Connector Versions

```powershell
git submodule foreach 'if [ "$name" != "src/connectors/tesseract" ]; then git checkout v0.12.0; fi'
git -C src/connectors/tesseract checkout v0.12.0-alpha.1
```

## Adding new file types

To make files of a new type available, add the new type to `build.resource.files` in docfx.json
