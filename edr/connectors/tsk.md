# EDR TSK® Connector

The Sleuth Kit Connector allows users to automate forensic workflows
using [The Sleuth Kit](https://www.sleuthkit.org/) and
[Autopsy](https://www.autopsy.com/).

This connector has [Steps](~/edr/steps/TSK.md) to:

- Create New Cases
- Add Data Sources To a Case
- Generate Case Reports
- Create a List of Case Data Sources

[TSK SCL examples available here](~/edr/examples/tsk.md).

Source code available on [GitLab](https://gitlab.com/reductech/edr/connectors/tsk).

The Steps all use the [Autopsy command line](http://sleuthkit.org/autopsy/docs/user-docs/4.19.0/command_line_ingest_page.html).

## TSK Connector Settings

The TSK Connector requires additional configuration which can be
provided using the `settings` key in `connectors.json`.

### Supported Settings

| Name        | Required |   Type   | Description                            |
| :---------- | :------: | :------: | :------------------------------------- |
| AutopsyPath |    ✔     | `string` | The Path the to the Autopsy Executable |

### Example Settings

```json
"Reductech.EDR.Connectors.TSK": {
  "id": "Reductech.EDR.Connectors.TSK",
  "enable": true,
  "version": "0.11.0",
  "settings": {
    "AutopsyPath": "C:\\Program Files\\Autopsy-4.19.1\\bin\\autopsy64.exe"
  }
}
```
