# EDR Singer Connector

The EDR Singer Connector allows users to import data from a [Singer](https://github.com/singer-io/getting-started) tap.

This connector has [Steps](~/edr/steps/Singer.md) to:

- Convert Singer data to entities

SCL examples available [here](~/edr/examples/singer.md).

Source code available on [GitLab](https://gitlab.com/reductech/edr/connectors/singer).
