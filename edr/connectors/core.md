# EDR Core

The `Core SDK` is:

- An interpreter for the [Sequence Configuration Language](~/edr/how-to/scl/sequence-configuration-language.md)
- A collection of [application-independent Steps](~/edr/steps/Core.md) that:
  - Can be used to import/export data and structure workflows
  - Work with various file formats: CSV, Json, Concordance
  - Manipulate strings, e.g. Append, Concatenate, ChangeCase
  - Enforce data standards and convert between various formats through the use of Schemas
  - Create and manipulate entities (structured objects that represent data)
  - Control flow, e.g. If, ForEach, While
- An SDK that allows [developers to create Connectors](~/edr/how-to/developers-guide.md)

To try Core in action, start with the [Quick Start](~/edr/how-to/quick-start.md) and
then try some of the [Examples](~/edr/examples/core.md).

[Core SCL examples available here](~/edr/examples/core.md).

Source code available on [GitLab](https://gitlab.com/reductech/edr/core).
