# EDR Structured Data Connector

This connector has [Steps](~/edr/steps/StructuredData.md)
to interact with structured data formats:

- CSV
- Concordance
- IDX
- JSON

SCL examples available [here](~/edr/examples/structureddata.md).

Source code available on [GitLab](https://gitlab.com/reductech/edr/connectors/structureddata).
