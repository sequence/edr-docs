# EDR PowerShell Connector

The PowerShell connector contains Steps for executing
inline PowerShell scripts as part of Sequences.

[PowerShell SCL examples available here](~/edr/examples/powershell.md).

Source code available on [GitLab](https://gitlab.com/reductech/edr/connectors/pwsh).
