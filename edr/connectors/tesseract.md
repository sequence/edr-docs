# EDR Tesseract Connector

This connector contains steps to perform optical character recognition (OCR)
on image files. It uses the [Tesseract](https://github.com/tesseract-ocr/tesseract)
open source library as the OCR engine.

SCL examples available [here](~/edr/examples/tesseract.md).

Source code available on [GitLab](https://gitlab.com/reductech/edr/connectors/tesseract).
