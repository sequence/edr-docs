# EDR SQL Connector

The EDR SQL Connector allows users to connect to SQL databases and manipulate tables.

This connector has [Steps](~/edr/steps/Sql.md) to:

- Build connection strings
- Create tables
- Insert entities into tables as rows
- Read entities from tables
- Run arbitrary queries

The following flavors of SQL are supported

- MariaDb
- MS SQL Server
- MySQL
- Postgres
- SQLite

SCL examples available [here](~/edr/examples/sql.md).

Source code available on [GitLab](https://gitlab.com/reductech/edr/connectors/sql).
