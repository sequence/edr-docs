# EDR File System Connector

This connector has [Steps](~/edr/steps/FileSystem.md) to:

- Read and write files
- Create, move, copy, and delete files
- Combine paths
- Extract compressed files

Source code available on [GitLab](https://gitlab.com/reductech/edr/connectors/filesystem).
