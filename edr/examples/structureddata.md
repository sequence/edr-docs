# Structured Data Connector Examples

## Setup

1. Download and set up EDR. See the [quick start](~/edr/how-to/quick-start.md) for more info.
2. Add the Structured Data connector is included by default.
   See [connectors](~/edr/how-to/quick-start.md#connectors) for more info on managing connectors.

## Examples

- [CSV Files](structureddata/csv-files.md)
- [EDRM Schema](structureddata/edrm-schema.md)
- [USSEC Schema](structureddata/ussec-schema.md)
