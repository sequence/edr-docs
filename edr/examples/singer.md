# Singer Connector Examples

## Setup

1. Download and set up EDR. See the [quick start](~/edr/how-to/quick-start.md) for more info.
2. Add the Singer connector. See [connectors](~/edr/how-to/quick-start.md#connectors)
   for more info on managing connectors.

```powershell
./edr.exe connector add Reductech.EDR.Connectors.Singer
```

## Examples

- [Import Data from Slack](singer/import-data-from-slack.md)
