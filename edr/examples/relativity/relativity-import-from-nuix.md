# EDR Connector for Relativity® - Import Entities From Nuix

This example will export entities from a Nuix Case and import them into a Relativity Workspace.

## Setup

- [Instructions on how to install and setup EDR and the EDR Connector for Relativity®.](~/edr/examples/relativity.md)
- [Instructions on how to install and setup EDR and the Nuix Connector.](~/edr/examples/nuix.md)

You will also need to create a .kwe file to map the properties.
To find out how to do this look at [Relativity Import Concordance.](relativity-import-concordance.md)

## SCL

Download the SCL here: [relativity-import-from-nuix.scl](scl/relativity-import-from-nuix.scl)

To run:

```powershell
PS > ./edr.exe run relativity-import-from-nuix.scl
```

```scala
- <NuixCasePath>        = 'c:/MyCase'
- <ProductionSet>       = 'MyProductionSet'
- <ExportPath>          = 'c:/MyCaseExport'
- <RelativityWorkspace> = 'My Workspace'
- <SettingsFilePath>    = 'c:/settingsFile.kwe'

- NuixExportConcordance
    CasePath: <NuixCasePath>
    ExportPath: <ExportPath>
    ProductionSet: <ProductionSet>
    TraversalStrategy: ExportTraversalStrategy.Items
    ExportOptions: (
      native: (path: 'NATIVE' naming: 'document_id')
      text: (path: 'TEXT' naming: 'document_id')
    )

- RelativityImport
    FilePath: (PathCombine [<ExportPath>, 'loadfile.dat'])
    SettingsFilePath: <SettingsFilePath>
    FileImportType: FileImportType.Object
    Workspace: <RelativityWorkspace>
```
