# EDR Connector for Relativity® - End to End Example

This example will

- Delete all existing Workspaces called 'Test Workspace'
- Create a Matter called 'Test Matter' if it doesn't already exist
- Create a Workspace called 'Test Workspace'
- Create a Fixed Length Field in the Workspace called 'Tags'

- Import some documents from a csv file
- Tag entities with a particular title
- Export the tagged entities to a new file

## Setup

[Instructions on how to install and setup EDR and the EDR Connector for Relativity®.](~/edr/examples/relativity.md)
To load the data from a .csv file you need to add the StructuredData and FileSystem connectors

## SCL

Download the SCL here: [relativity-end-to-end.scl](scl/relativity-end-to-end.scl)
Download the example entity import here: [relativity-example-files.zip](relativity-example-files.zip)

To run:

```powershell
PS > ./edr.exe run relativity-end-to-end.scl
```
