# EDR Connector for Relativity® - Search and Tag Items

This example will run a search of a Relativity workspace and set the 'Tags' field of each of the results.
Be sure that the field you are setting (e.g. 'Tags') exists in your workspace before running this.

## Setup

[Instructions on how to install and setup EDR and the EDR Connector for Relativity®.](~/edr/examples/relativity.md)

## SCL

Download the SCL here: [relativity-search-and-tag.scl](scl/relativity-search-and-tag.scl)

To run:

```powershell
PS > ./edr.exe run relativity-search-and-tag.scl
```

```scala
# Set the name of your workspace. You could also use Workspace Artifact Id
- <WorkspaceName> = 'Test Workspace'
- <Search>        = "'Title' LIKE 'Clouds'"  # Define your search here
- <Tag>           = 'Important'

- <FilesToTag> = RelativitySendQuery
                   Workspace: <WorkspaceName>
                   Condition: <Search>
                   Start: 0
                   Length: 100 # The max number of objects to tag

- ForEach <FilesToTag> Action: (
    RelativityUpdateObject
      Workspace: <WorkspaceName>
      ObjectArtifactId: <>["ArtifactId"]
      FieldValues: ('Tags': <Tag>)
  )

- Log $"Tagged {ArrayLength <FilesToTag>} Files."
```
