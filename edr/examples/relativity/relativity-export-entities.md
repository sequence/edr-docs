# EDR Connector for Relativity® - Export Entities

This example will export entities from a Relativity Workspace and write them to a .csv file

## Setup

[Instructions on how to install and setup EDR and the EDR Connector for Relativity®.](~/edr/examples/relativity.md)

To write the data to a .csv file you need to add the StructuredData and FileSystem connectors

## SCL

Download the SCL here: [relativity-export-entities.scl](scl/relativity-export-entities.scl)

To run:

```powershell
PS > ./edr.exe run relativity-export-entities.scl
```

```scala
# Set the name of your workspace. You could also use Workspace Artifact Id
- <WorkspaceName> = 'Test Workspace'
- <ExportFilter>  = "'Tags' LIKE 'Important'"
- <OutputPath>    = 'entities.csv'

# Export the entities
- <ExportedEntities> = RelativityExport
                         Workspace: <WorkspaceName>
                         Condition: <ExportFilter>
                         FieldNames: ['Title', 'Tags']

# Define a schema
- <Schema> = (
    'Name': "ValueIf Schema"
    'Properties': (
      'Title': ('Type': SCLType.String 'Multiplicity': Multiplicity.ExactlyOne)
      'Tags':  ('Type': SCLType.String 'Multiplicity': Multiplicity.ExactlyOne)
    )
  )

# Apply the schema to the entities
- <MappedExportedEntities> = EnforceSchema EntityStream: <ExportedEntities> Schema: <Schema>
| ArrayEvaluate

# Write the entities to a csv file
- <MappedExportedEntities> | ToCSV | Log
- <MappedExportedEntities> | ToCSV | FileWrite <OutputPath>
- Log $"Exported {ArrayLength <MappedExportedEntities>} Entities."
```
