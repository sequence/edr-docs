# Generate EDR Documentation

We use SCL to generate the [EDR Steps](~/edr/steps/all.md) documentation.
Here's how it's done:

```perl
- <root> = 'Documentation'
- <docs> = GenerateDocumentation

# Create a directory for each connector
- <docs>
  | ArrayDistinct (From <> 'Directory')
  | ForEach (
      CreateDirectory (PathCombine [<root>, (From <> 'Directory')])
    )

# Export all steps to .\<root>\ConnectorName\StepName.md
- <docs>
  | Foreach (
      FileWrite
        (From <> 'FileText')
        (PathCombine [<root>, (From <> 'Directory'), (From <> 'FileName')])
    )
```
