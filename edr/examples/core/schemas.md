# Schema Examples

More details on schemas [here](~/edr/how-to/scl/schemas.md).

## Write Schema to JSON

This example creates a schema and writes it to a JSON file.

This schema enforces that every entity has

- An 'Artist' property of type `String`
- An 'ArtistId' property of type `Integer`
- A date with the format `yyyy`, however when dates are missing, only a warning is emitted

```perl
- <schema> = (
    Name: 'Artwork Artist Schema'
    AllowExtraProperties: true
    Properties:(
      artist: (Type: 'String' Multiplicity: 'ExactlyOne')
      artistId: (Type: 'Integer' Multiplicity: 'ExactlyOne')
	  year: (
        Type: 'Date'
        Multiplicity: 'UpToOne'
        DateInputFormats: ['yyyy']
        ErrorBehavior: 'Warning'
      )
    )
  )
- <schema> | ToJson | FileWrite 'artwork-schema.json'
```

The JSON file created looks like this

```JSON
{
  "Name": "Artwork Artist Schema",
  "AllowExtraProperties": true,
  "Properties": {
    "artist": { "Type": "String", "Multiplicity": "ExactlyOne" },
    "artistId": { "Type": "Integer", "Multiplicity": "ExactlyOne" },
    "year": {
      "Type": "Date",
      "Multiplicity": "UpToOne",
      "DateInputFormats": ["yyyy"],
      "ErrorBehavior": "Warning"
    }
  }
}
```

## Enforce Schema

This example:

1. Reads a schema from a JSON File (created in the previous example)
2. Reads entities from a CSV file and ensures that they all match the given schema
3. Prints out the number of entities validated

If any entities do not match the schema, an error will be printed.

The `artwork_data.csv` file can be downloaded using the
[PowerShell - Download CSV](~/edr/examples/powershell/download-csv.md) example or
from [here](https://github.com/tategallery/collection/blob/master/artwork_data.csv).

```perl
- <schema> = FileRead 'artwork-schema.json' | FromJson
- <count> = FileRead 'artwork_data.csv'
| FromCSV
| EnforceSchema <schema> ErrorBehavior: 'Warning'
| ArrayLength
- $"Successfully validated: {<count>}"
```
