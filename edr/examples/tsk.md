# EDR Connector for TSK Examples

## Setup

1. Download and set up EDR. See the [quick start](~/edr/how-to/quick-start.md) for more info.
2. Add the TSK connector. See [connectors](~/edr/how-to/quick-start.md#connectors)
   for more info on managing connectors.

```poweshell
PS > .\edr.exe connector add Reductech.EDR.Connectors.TSK
```

3. Add the required TSK configuration to `./lib/connectors.json`.

See [readme](~/edr/connectors/tsk.md) for information about connector settings.

## Examples

### [Create Case and Add Data](tsk/tsk-create-case-and-add-data.md)

Create a new case and add data to it

### [Create Reports](tsk/tsk-create-reports.md)

Create Reports from a case

### [Import from Nuix](tsk/tsk-import-from-nuix.md)

An example of creating a case in nuix, importing data into it, exporting that data, and then importing that data into Autopsy
