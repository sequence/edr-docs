# USSEC Schema

This is a schema which enforces that data meets the
[EDRM Production Standards](https://edrm.net/resources/frameworks-and-standards/edrm-model/edrm-stages-standards/edrm-production-standards-version-1/).

```scala


(
	'Name': "EDRM Production Standards"
	'Properties': (
		'ATTACHMENTIDS': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'AUTHORS': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'BATES RANGE': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'BCC': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
		'CC': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
		'CUSTODIAN': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'DATECREATED': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "yyyy/MM/dd"
		)
		'DATERECEIVED': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "yyyy/MM/dd"
		)
		'DATESAVED': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "yyyy/MM/dd"
		)
		'DATESENT': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "yyyy/MM/dd"
		)
		'DOCEXT': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'DOCID': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'DOCLINK': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'DOCTITLE': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'FILENAME': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'FOLDER': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'FROM': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'HASH': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'PARENTID': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'RCRDTYPE': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'SUBJECT': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'TIMERECEIVED': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "HH:mm:ss zzz"
		)
		'TIMESENT': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "HH:mm:ss zzz"
		)
		'TO': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
	)
	'ExtraProperties': ExtraPropertyBehavior.Warn
	'DefaultErrorBehavior': ErrorBehavior.Error
)




```
