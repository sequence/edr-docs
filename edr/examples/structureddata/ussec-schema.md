# USSEC Schema

This is a schema which enforces that data meets the
[United States Securities and Exchange Commision data standards](https://www.sec.gov/divisions/enforce/datadeliverystandards.pdf).

```scala

(
	'Name': "United States Securities and Exchange Commission Data Delivery Standards"
	'Properties': (
		'ATTACHRANGE': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'AUTHOR': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'BCC': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
		'BEGATTACH': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'CC': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
		'CHILD_BATES': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
		'CUSTODIAN': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'DATE_ACCESSD': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "MM/dd/yyyy"
		)
		'DATE_CREATED': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "MM/dd/yyyy"
		)
		'DATE_MOD': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "MM/dd/yyyy"
		)
		'DATE_SENT': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "MM/dd/yyyy"
		)
		'ENDATTACH': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'FILE_EXTEN': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'FILE_NAME': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'FILE_SIZE': (
			'Type': SCLType.Integer
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'FIRSTBATES': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'FROM': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
		'HEADER': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'INTFILEPATH': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'INTMSGID': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'LAST_AUTHOR': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'LASTBATES': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'LINK': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'MD5HASH': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
			'Regex': "[0-9a-f]+"
		)
		'MIME_TYPE': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'OCRPATH': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'PARENT_BATES': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'PATH': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'PGCOUNT': (
			'Type': SCLType.Integer
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'PRINTED_DATE': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "MM/dd/yyyy"
		)
		'SUBJECT': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.ExactlyOne
		)
		'TIME_ACCESSD/TIME_ZONE': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "hh:mm tt zzz"
		)
		'TIME_CREATED/TIME_ZONE': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "hh:mm tt zzz"
		)
		'TIME_MOD/TIME_ZONE': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "hh:mm tt zzz"
		)
		'TIME_SENT/TIME_ZONE': (
			'Type': SCLType.Date
			'Multiplicity': Multiplicity.UpToOne
			'DateOutputFormat': "hh:mm tt zzz"
		)
		'TIME_ZONE': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.UpToOne
		)
		'TO': (
			'Type': SCLType.String
			'Multiplicity': Multiplicity.Any
		)
	)
	'ExtraProperties': ExtraPropertyBehavior.Remove
	'DefaultErrorBehavior': ErrorBehavior.Error
)

```
