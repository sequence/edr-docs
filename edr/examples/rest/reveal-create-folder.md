# EDR Connector for REST - Create Reveal Folder

This example will

- Login to Reveal
- Create a new folder

## Setup

[Instructions on how to install and setup EDR and the EDR Connector for Rest.](~/edr/examples/rest.md)

## SCL

Download the SCL here: [reveal-reveal-create-folder.scl](scl/reveal-create-folder.scl)

To run:

```powershell
PS > ./edr.exe run reveal-reveal-create-folder.scl
```

```scala
- <username> = 'username'
- <password> = 'password'

- <caseId> = '159'
- <folderBody> = ("Name": "MyTestFolder" )

- <session> = Reveal_Login_Get username: <username> password: <password>
- <userId> = <session>["UserId"]
- <LoginSessionId> = <session>["LoginSessionId"]

- <folderCreateResult> = (Reveal_WorkFolder_Create Body: <folderBody> caseId: <caseId> InControlAuthToken: <LoginSessionId> userId: <userId>)
```
