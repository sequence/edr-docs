# EDR Connector for REST - Get Reveal Case Statistics

This example will

- Login to Reveal
- Get statistics for a particular case

## Setup

[Instructions on how to install and setup EDR and the EDR Connector for Rest.](~/edr/examples/rest.md)

## SCL

Download the SCL here: [reveal-get-case-statistics.scl](scl/reveal-get-case-statistics.scl)

To run:

```powershell
PS > ./edr.exe run reveal-get-case-statistics.scl
```

```scala
- <username> = 'username'
- <password> = 'password'

- <session> = Reveal_Login_Get username: <username> password: <password>
- <userId> = <session>["UserId"]
- <LoginSessionId> = <session>["LoginSessionId"]
- <caseId> = 159
- <requestBody> = (  "CaseId": <caseId>,  "LatestOnly": true)
- Reveal_Cases_GetCaseStatistics InControlAuthToken: <LoginSessionId> Body: <requestBody>
```
