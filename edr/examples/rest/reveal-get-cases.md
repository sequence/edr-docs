# EDR Connector for REST - Get Reveal Cases

This example will

- Login to Reveal
- Get a list of Reveal cases

## Setup

[Instructions on how to install and setup EDR and the EDR Connector for Rest.](~/edr/examples/rest.md)

## SCL

Download the SCL here: [reveal-get-cases.scl](scl/reveal-get-cases.scl)

To run:

```powershell
PS > ./edr.exe run reveal-get-cases.scl
```

```scala
- <username> = 'username'
- <password> = 'password'

- <session> = Reveal_Login_Get username: <username> password: <password>
- <userId> = <session>["UserId"]
- <LoginSessionId> = <session>["LoginSessionId"]
- Reveal_Cases_Get InControlAuthToken: <LoginSessionId>
```
