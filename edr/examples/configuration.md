# EDR Configuration Examples

EDR is configured using the `appsettings.json` file which can be found
in the `lib` directory.

| File                                                                              | Description                                                                                                      |
| :-------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------------- |
| [appsettings-default.json](~/edr/examples/configuration/appsettings-default.json) | The default configuration file that comes with EDR. Logs to Console (`Info`) and File (`Debug`)                  |
| [appsettings-docgen.json](~/edr/examples/configuration/appsettings-docgen.json)   | Minimal configuration that only outputs warnings and errors to the console. It's used to generate documentation. |
| [appsettings-full.json](~/edr/examples/configuration/appsettings-full.json)       | Full configuration that includes four logging targets: Console, File, JSON file, and Elastic Search.             |
