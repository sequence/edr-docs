# Tesseract Connector Examples

## Setup

1. Download and set up EDR. See the [quick start](~/edr/how-to/quick-start.md) for more info.
2. Add the Tesseract connector. See [connectors](~/edr/how-to/quick-start.md#connectors)
   for more info on managing connectors.

```powershell
./edr.exe connector add Reductech.EDR.Connectors.Tesseract --prerelease
```

## Examples

### OCR a bitmap image

```perl
- <path> = 'MyImage.bmp'
- <imageData> = FileRead <path>
- <imageFormat> = GetImageFormat <path>
- <imageText> = TesseractOCR <imageData> <imageFormat>
- Print <imageText>
```
