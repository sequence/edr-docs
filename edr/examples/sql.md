# SQL Connector Examples

## Setup

1. Download and set up EDR. See the [quick start](~/edr/how-to/quick-start.md) for more info.
2. Add the SQL connector. See [connectors](~/edr/how-to/quick-start.md#connectors)
   for more info on managing connectors.

```powershell
./edr.exe connector add Reductech.EDR.Connectors.Sql
```

## Examples

- [Create SQL Table and Insert Data](sql/create-table-insert-data.md)
- [Read Data from a SQL Table](sql/read-data-from-table.md)
