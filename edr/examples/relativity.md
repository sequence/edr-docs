# EDR Connector for Relativity® Examples

## Setup

1. Download and set up EDR. See the [quick start](~/edr/how-to/quick-start.md) for more info.
2. Add the EDR Connector for Relativity®. See [connectors](~/edr/how-to/quick-start.md#connectors)
   for more info on managing connectors.

```poweshell
PS > .\edr.exe connector add Reductech.EDR.Connectors.Relativity
```

3. Add the required Relativity® configuration to `./lib/connectors.json`.
   See [EDR Connector for Relativity®](~/edr/connectors/relativity.md#edr-connector-for-relativity-settings)

## Examples

### [Create Workspace](relativity/relativity-create-workspace.md)

Create a new workspace and add fields to it.

### [Search and Tag Items](relativity/relativity-search-and-tag.md)

An example of how to search and tag items in an existing workspace.

### [Export Entities](relativity/relativity-export-entities.md)

An example of how to export documents as entities.

### [Import Entities](relativity/relativity-import-entities.md)

An example of how to import entities into an existing workspace as documents.

### [Import Concordance](relativity/relativity-import-concordance.md)

An example of how to import a concordance file into a workspace.

### [Export from Nuix and Import into Relativity](relativity/relativity-import-from-nuix.md)

An example of how to export data from Nuix and import it into a Relativity Workspace.

### [End-to-end Example](relativity/relativity-end-to-end.md)

A combination of the above examples - create workspace, import data, search and tag, then export.
