# TSK - Create a Case and Add Data

This example will

- Create a new TSK Case
- Add a data source to that case
- Ingest the data

## Setup

[Instructions on how to install and setup EDR and the TSK Connector.](~/edr/examples/tsk.md)

To run:

```powershell
PS > ./edr.exe run tsk-create-case-and-add-data.scl
```

## SCL

Download the SCL here: [tsk-create-case-and-add-data.scl](scl/tsk-create-case-and-add-data.scl)

```scala
- AutopsyCreateNewCase
    CaseName: 'MyNewCase'
    CaseBaseDirectory: (PathCombine ['CaseDirectory'] )
    DataSourcePath: (PathCombine ['dataExamples', 'loadfile_0001-10001.dat'])
    IngestProfileName: '' #Use Default Profile
```
