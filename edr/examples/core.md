# SCL / Core Examples

## Setup

See the [quick start](~/edr/how-to/quick-start.md) for more info on
how to download and set up EDR.

## Examples

Here are some examples of what you can do with SCL.

Many of them use the Tate Collection metadata https://github.com/tategallery/collection

- [Generate Documentation](core/generate-documentation.md)
- [Schemas](core/schemas.md)
