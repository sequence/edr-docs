# Create SQL Table and Insert Data

[Instructions on how to install and setup EDR and the SQL Connector.](~/edr/examples/sql.md)

This workflow:

1. Drops the table 'MyTable' if it exists
2. Creates a new Table 'MyTable'
3. Inserts data into 'MyTable'

To try out this workflow, you'll need an instance of MariaDb running
on `localhost:3306`. You can use a Docker container for this:

```bash
docker run -d -p 3306:3306 --name mariadb -e MYSQL_ROOT_PASSWORD=verysecret -e MYSQL_DATABASE=database mariadb
```

## SCL

```scala
- <ConnectionString> = CreateMySQLConnectionString
                         Server: 'localhost'
                         Database: 'database'
                         UserName: 'root'
                         Password: 'verysecret'

- <TableName> = 'Config'
- <Schema> = (
    Name: <TableName>
    AllowExtraProperties: False
    Properties: (
      Id: (
        Type: SCLType.Integer
        Multiplicity: Multiplicity.ExactlyOne
      )
      SourcePath: (
        Type: SCLType.String
        Multiplicity: Multiplicity.ExactlyOne
      )
      DestinationPath: (
        Type: SCLType.String
        Multiplicity: Multiplicity.ExactlyOne
      )
    )
  )

- SqlCommand
    ConnectionString: <ConnectionString>
    DatabaseType: DatabaseType.MariaDb
    Command: $"DROP TABLE IF EXISTS {<TableName>}"

- SqlCreateTable
    ConnectionString: <ConnectionString>
    DatabaseType: DatabaseType.MariaDb
    Schema: <Schema>

- SqlInsert
    ConnectionString: <ConnectionString>
    DatabaseType: DatabaseType.MariaDb
    Entities: [
      (Id: 1 SourcePath: 'C:\temp\file1.txt' DestinationPath: 'C:\temp\file1-2.txt' )
      (Id: 2 SourcePath: 'C:\temp\file2.txt' DestinationPath: 'C:\temp\file2-2.txt' )
    ]
    Schema: <Schema>
```
