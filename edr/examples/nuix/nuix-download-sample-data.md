# Nuix - Download Sample Data

This SCL will:

1. Create a new directory `./data` in the current directory
2. Download the [EDRM Micro Dataset](https://edrm.net/resources/data-sets/edrm-micro-datasets/)
   and extract it into the `./data` directory.
3. Download an [encrypted PDF from the Nuix.Tests project](https://gitlab.com/reductech/edr/connectors/nuix/-/blob/master/Nuix.Tests/AllData/encrypted.pdf)
   into the `./data/EDRM specific data` directory.

## Setup

[Instructions on how to install and setup EDR and the Nuix Connector.](~/edr/examples/nuix.md)

To run this SCL, add the [PowerShell connector](~/edr/connectors/powershell.md) by running:

```powershell
./edr.exe connector add Reductech.EDR.Connectors.Pwsh
```

## SCL

Download the SCL here: [nuix-download-sample-data.scl](scl/nuix-download-sample-data.scl)

To run:

```powershell
PS > ./edr.exe run nuix-download-sample-data.scl
```

```scala
- <output> = './data'
- AssertTrue (Not (DoesDirectoryExist <output>))
- If (Not (DoesDirectoryExist <output>)) (CreateDirectory <output>)
- <edrm> = PathCombine [<output>, 'edrm.zip']
- <encrypted> = PathCombine [<output>, 'encrypted.pdf']
- PwshRunScript
    Variables: (
      EdrmMicroUrl: 'https://www.edrm.net/download/28896/'
      EdrmMicroPath: <edrm>
      EncryptedUrl: 'https://gitlab.com/reductech/edr/connectors/nuix/-/raw/master/Nuix.Tests/AllData/encrypted.pdf?inline=false'
      EncryptedPath: <encrypted>
    )
    Script: '
      Invoke-WebRequest -Uri $EdrmMicroUrl -OutFile $EdrmMicroPath -UseBasicParsing
      Invoke-WebRequest -Uri $EncryptedUrl -OutFile $EncryptedPath -UseBasicParsing
    '
- FileExtract <edrm> <output>
- DeleteItem <edrm>
- AssertTrue (DoesDirectoryExist (PathCombine [<output>, 'EDRM specific data']))
- FileMove <encrypted> (PathCombine [<output>, 'EDRM specific data/encrypted.pdf'])
- AssertTrue (DoesDirectoryExist (PathCombine [<output>, 'Data from public websites']))
- Log $"Successfully downloaded example data to {<output>}"
```
