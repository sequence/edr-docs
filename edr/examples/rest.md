These are examples of how to use the EDR REST connector.

The connector works by generating steps from an OpenAPI specification.

The examples here use the [Reveal Specification](https://salient-eu.revealdata.com/rest/swagger/docs/V2) to connect to the Reveal API.

## Setup

1. Download and set up EDR. See the [quick start](~/edr/how-to/quick-start.md) for more info.
2. Add the EDR Connector for REST. See [connectors](~/edr/how-to/quick-start.md#connectors)
   for more info on managing connectors.

```poweshell
PS > .\edr.exe connector add Reductech.EDR.Connectors.Rest
```

3. Add the required configuration to `./lib/connectors.json`.
   See [EDR Connector for REST](~/edr/connectors/rest.md)

## Examples

### [Create Folder](rest/reveal-create-folder.md)

Login to Reveal and create a work folder.

### [Get Cases](rest/reveal-get-cases.md)

Login to Reveal and get a list of cases.

### [Get Cases](rest/reveal-get-case-statistics.md)

Login to Reveal and get statistics for a case.
