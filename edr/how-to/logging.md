# Logging

## NLog

EDR uses [NLog](https://nlog-project.org) for writing log messages
to various targets, including:

- Console
- File
- JSON
- Database
- Elastic Search

The NLog section of `appsettings.json` controls the format, detail level,
and logging targets.

The default nlog configuration supplied with EDR writes to Console and to
a file called `edr.log` in the same directory as the EDR executable.
Minimum console severity is `Info` and file is `Debug`.

```json
"nlog": {
  "throwConfigExceptions": true,
  "variables": {
    "edrlogname": "edr"
  },
  "targets": {
    "fileTarget": {
      "type": "File",
      "fileName": "${basedir:fixtempdir=true}\\${edrlogname}.log",
      "layout": "${date} ${level:uppercase=true} ${message} ${exception}"
    },
    "consoleInfo": {
      "type": "Console",
      "layout": "${date} ${message}"
    },
    "consoleError": {
      "type": "Console",
      "layout": "${date} ${level:uppercase=true} ${message}",
      "error": true
    }
  },
  "rules": [
    {
      "logger": "*",
      "minLevel": "Error",
      "writeTo": "fileTarget,consoleError",
      "final": true
    },
    {
      "logger": "*",
      "minLevel": "Info",
      "writeTo": "consoleInfo"
    },
    {
      "logger": "*",
      "minLevel": "Debug",
      "writeTo": "fileTarget"
    }
  ]
}
```

Please see the [NLog documentation](https://github.com/NLog/NLog.Extensions.Logging/wiki/NLog-configuration-with-appsettings.json)
for more details on how to configure various log targets.

## Log Messages

Core uses _structured logging_ across its Steps and Connectors.
Each log message has additional metadata appended to it to improve
auditability and help troubleshoot issues:

| Property      | Description                                                                                                                                                         |
| :------------ | :------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Message       | The information or error message.                                                                                                                                   |
| MessageParams | Additional properties associated with the `Message`. For example, when logging that a step has successfully completed, one of the MessageParams is the result type. |
| StepName      | The name of the Step that has logged the message. Empty if the message is outside of a step e.g. when EDR starts.                                                   |
| Location      | Location information detailing where in a Sequence the message has been logged. A stack trace of sorts.                                                             |
| SequenceInfo  | A dictionary of additional properties associated with the running Step and/or Sequence.                                                                             |

### Logging Scopes

Sometimes it might be difficult to determing where in the execution
tree a particular log or error message might be coming from.

You can solve this problem by looking at the scope of the message.

To include scopes in the logging output, use the [ndlc layour renderer](https://github.com/NLog/NLog/wiki/NDLC-Layout-Renderer)

For example:

```json
"consoleInfo": {
  "type": "Console",
  "layout": "${date} ${message} (${ndlc:separator=\\\\})"
}
```

Will produce the following output (if log level is `Trace`):

```
> edr.exe run scl "Print (2 + (3 * 4))"

2021/03/04 12:01:51.225 EDR Sequence Started (EDR)
2021/03/04 12:01:51.257 Print Started with Parameters: [Value, Sum] (EDR\Print)
2021/03/04 12:01:51.265 Sum Started with Parameters: [Terms, ArrayNew] (EDR\Print\Sum)
2021/03/04 12:01:51.265 ArrayNew Started with Parameters: [Elements, 2 Elements] (EDR\Print\Sum\ArrayNew)
2021/03/04 12:01:51.265 Product Started with Parameters: [Terms, ArrayNew] (EDR\Print\Sum\ArrayNew\Product)
2021/03/04 12:01:51.278 ArrayNew Started with Parameters: [Elements, 2 Elements] (EDR\Print\Sum\ArrayNew\Product\ArrayNew)
2021/03/04 12:01:51.278 ArrayNew Completed Successfully with Result: 2 Elements (EDR\Print\Sum\ArrayNew\Product\ArrayNew)
2021/03/04 12:01:51.298 Product Completed Successfully with Result: 12 (EDR\Print\Sum\ArrayNew\Product)
2021/03/04 12:01:51.298 ArrayNew Completed Successfully with Result: 2 Elements (EDR\Print\Sum\ArrayNew)
2021/03/04 12:01:51.298 Sum Completed Successfully with Result: 14 (EDR\Print\Sum)
14
2021/03/04 12:01:51.298 Print Completed Successfully with Result: Unit (EDR\Print)
2021/03/04 12:01:51.298 EDR Sequence Completed (EDR)
```

## Full Example

This configuration logs to:

- Console
- File - `edr.log`
- JSON file - `edr.log.json`
- Elastic Search - `http://localhost:9200`

```json
"nlog": {
  "throwConfigExceptions": true,
  "variables": { "edrlogname": "edr" },
  "extensions": [{ "assembly": "NLog.Targets.ElasticSearch" }],
  "targets": {
    "jsonTarget": {
      "type": "File",
      "fileName": "${basedir:fixtempdir=true}\\${edrlogname}.log.json",

      "layout": {
        "type": "JsonLayout",
        "includemdlc": true,
        "Attributes": [
          { "name": "level", "layout": "${level}" },
          { "name": "ts", "layout": "${date}" },
          { "name": "caller", "layout": "${ndlc:separator=\\\\}" },
          { "name": "msg", "layout": "${message}" },
          {
            "name": "properties",
            "encode": false,
            "layout": {
              "type": "JsonLayout",
              "includeallproperties": "true"
            }
          }
        ]
      }
    },
    "fileTarget": {
      "type": "File",
      "fileName": "${basedir:fixtempdir=true}\\${edrlogname}.log",
      "layout": "${date} ${level:uppercase=true} ${message} ${exception}"
    },
    "consoleInfo": {
      "type": "Console",
      "layout": "${date} ${message}"
    },
    "consoleError": {
      "type": "Console",
      "layout": "${date} ${level:uppercase=true} ${message}",
      "error": true
    },
    "elastic": {
      "type": "BufferingWrapper",
      "flushTimeout": "5000",
      "target": {
        "type": "ElasticSearch",
        "uri": "http://localhost:9200",
        "index": "${edrlogname}-${date:format=yyyy.MM.dd}",
        "fields": [
          { "name": "level", "layout": "${level:uppercase=true}" },
          {
            "name": "stepName",
            "layout": "${event-properties:StepName}"
          },
          { "name": "message", "layout": "${message}" },
          {
            "name": "result",
            "layout": "${event-properties:Result}"
          },
          {
            "name": "parameters",
            "layout": "${event-properties:Parameters}"
          },
          {
            "name": "error",
            "layout": "${event-properties:Error}"
          },
          {
            "name": "location",
            "layout": "${event-properties:Location:objectpath=AsString}"
          }
        ]
      }
    }
  },
  "rules": [
    {
      "logger": "*",
      "minLevel": "Error",
      "writeTo": "fileTarget,consoleError,jsonTarget,elastic",
      "final": true
    },
    {
      "logger": "*",
      "minLevel": "Trace",
      "writeTo": "consoleInfo"
    },
    {
      "logger": "*",
      "minLevel": "Trace",
      "writeTo": "fileTarget,jsonTarget,elastic"
    }
  ]
}
```
