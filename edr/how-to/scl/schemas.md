# Schemas

Schemas are a way to ensure that your data has a particular structure, for example you can control which properties are present and what types they have.


### Steps which use JSON Schema:

- `CreateSchema` creates a schema from an array of entities. 

The step will produce the most restrictive schema possible.

```scala
- CreateSchema [('Foo': 1), ('Foo': 2)]
| Log
```

Please note that this step is quite expensive so if you have a large array of entities you should use `ArrayTake 100` to use only the first hundred entities to create the Schema.

- `Transform` tries to adjust entities so that they fit a schema.

This example transforms the 'Foo' value from a string to an integer.

```scala
- <schema> = FromJSON '{"type": "object", "properties": {"foo": {"type": "integer"}}}'
- <entities> = [('Foo': '1'), ('Foo': '2'), ('Foo': '3')]
- <results> = Transform <entities> <schema>
- <results> | ForEach | Log
```



You can provide additional arguments to control 

- `Validate` ensures that every entity in an array exactly matches the schema

This example filters the entities to only those where 'Foo' is a multiple of 2.

You could change the Error Behavior to do act differently for elements which do not match.
```scala
- <schema> = FromJSON '{"type": "object", "properties": {"foo": {"type": "integer", "MultipleOf": 2}}}'
- <entities> = [('Foo': 1), ('Foo': 2), ('Foo': 3), ('Foo': 4)]
- <results> = Validate <entities> <schema> ErrorBehavior: 'Skip'
- <results> | ForEach | Log
```

## Error Behavior

In the `Transform` and `Validate` steps, when there are entities that fail validation, the output depends on the argument passed to the `ErrorBehavior` parameters.

The possible values for [`ErrorBehavior`](~/edr/steps/Enums/ErrorBehavior.md) are:

| Value   | Description                                                             |
| ------- | ----------------------------------------------------------------------- |
| Fail    | Stop at the first error that is encountered                             |
| Error   | Emit non-terminating error and filter out entities that fail validation |
| Warning | Emit warning but do nothing                                             |
| Skip    | Filter out entities but do not emit warning                             |
| Ignore  | Ignore                                                                  |


### Resources for JSON Schemas:

- [Official Website](https://json-schema.org/)
- [JSON Schema Reference](https://json-schema.org/understanding-json-schema/reference/index.html)
- [Online Validator](https://www.jsonschemavalidator.net/)

