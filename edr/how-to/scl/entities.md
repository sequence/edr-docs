# Entities

An entity is a collection of property-value or key-value pairs.

You can define entities with round brackets:

```perl
(Property: 'Value')
```

Entities can have any number of properties

```perl
(Property: 'Value' Key: 'AnotherValue' NumberProperty: 123)
```

You can quote property names to include spaces and other characters:

```perl
('My Property': 123)
```

Entity values can have any [type](sequence-configuration-language.md#types), including arrays and entities.

```perl
(
  Name: 'Foo'
  Letters: ['F','o','o']
  Friend: (
    Name: 'Bar'
    Letters: ['B','a','r']
  )
)
```

# Getting values

To get a property value from an entity use the `EntityGetValue` step.

```perl
- <entity> = (Foo: 'a' Bar: 'b')
- EntityGetValue <entity> 'Foo' # returns: 'a'
```

A shorthand for `EntityGetValue` is square brackets:

```perl
- <entity> = (Foo: 'a' Bar: 'b')
- <entity>['Foo'] # returns: 'a'

- 5 * (Number: 5)['Number'] # returns: 25
```

To retrieve nested entities, use nested square brackets:

```perl
- <entity> = (Foo: (Bar: 'a'))
- <entity>['Foo']['Bar'] # returns: 'a'
```

# Nested properties

You can use `.`s in property names to nest properties:

```perl
#The following are all equivalent
- (a: (b: (c: 123)))
- (a.b.c: 123)
- ('a'.'b'.'c': 123)
- (a.'b'.c: 123)
```

And also to retrieve property values using the same notation:

```perl
- <entity> = (a: (b: (c: 123)))
- <entity>['a.b.c'] # returns: 123
```

# Combining Properties

You can combine entities with the `+` operator.

Nested entities will be combined but if a primitive appears on both then the value on the rightmost entity will be used.

```perl
- (Foo: 'a')               + (Bar: 'b')      # (Foo: 'a' Bar: 'b')
- (Foo: 'a' Bar: 'b')      + (Bar: 'c')      # (Foo: 'a' Bar: 'c')
- (x: (Foo: 'a'))          + (x: (Bar: 'b')) # (x:(Foo: 'a' Bar: 'b'))
- (x: (Foo: 'a' Bar: 'b')) + (x: (Bar: 'c')) # (x:(Foo: 'a' Bar: 'c'))
```

# Entity Arrays

A primary use case of SCL is handling sequential collections of entities.

It is easy to read and write different formats like CSV, Concordance, and JSON.

There are also steps for filtering, mapping, sorting, and distincting entities.
See [steps prefixed with `Entity`](~/edr/steps/Core/EntityCombine.md) for more details.

```perl
FileRead 'artwork_data.csv'
| FromCSV # Convert CSV into a stream of entities
| Filter (<>['artist'] == 'Blake, Robert') # Filter the stream
# Change the artist's name to uppercase:
| EntityMap (in <> set: 'artist' to: (StringToCase (from <> 'artist') TextCase.Upper))
# Rename properties from 'artist' to 'Artist Name' and 'artistId' to 'Artist Id':
| EntityMapProperties (artist: 'Artist Name' artistId: 'Artist Id')
| Sort (<>['year']) # sort by year
| Distinct (<>['id']) # remove duplicate entries using the 'id' as the key
| ToJsonArray # convert the entity stream to JSON
| FileWrite 'artwork_data.json'
```

Some steps Require a function argument which will be called on every element of the array.
For example the `Filter` function filters an array to only elements which meet a particular condition and that condition must be provided as an argument.

The function you provide can access the member of the array which is being evaluated in three ways, all of which are equivalent:

```perl
- <elements> = FileRead 'artwork_data.csv'| FromCSV # Convert CSV into a stream of entities
- <elements> | Filter (<item>['artist'] == 'Blake, Robert') | Log # Use the variable named <item>>
- <elements> | Filter (<myvariable> => <myvariable>['artist'] == 'Blake, Robert') | Log # Rename the variable <myvariable>
- <elements> | Filter (<>['artist'] == 'Blake, Robert') | Log # Use the automatic variable <>
```