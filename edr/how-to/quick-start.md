# Quick Start

1. Download the latest release [here](https://gitlab.com/reductech/edr/edr/-/releases)
2. Unzip the archive and open a shell (cmd, pwsh, powershell) of your choice in that directory
3. Run `.\edr.exe run scl "Print 'Hello world'"`

## Running SCL

To run a Sequence from a file:

```powershell
PS > .\edr.exe run .\sequence.scl
```

From the command line:

```powershell
PS > .\edr.exe run scl "- <version> = GetApplicationVersion `n- Print <version>"
```

## Help

To display the available commands and parameters when running edr, use the
`--help` or `-h` argument:

```powershell
PS > .\edr.exe --help
```

To see a list of all the `Steps` available, use the `steps` command:

```powershell
PS > .\edr.exe steps

# To filter by name or connector, add a filter as the first argument
PS > .\edr.exe steps file
```

## Connectors

EDR uses a connector system to extend functionality to various applications.

By default, EDR comes with the `FileSystem` and `StructuredData` connectors.
All the available connectors can be seen in the
[Connector Registry](https://gitlab.com/reductech/edr/connector-registry/-/packages).

To manage connectors, use the `connector` command:

```powershell
PS > .\edr.exe connector

# To list the connectors currently installed, use list
PS > .\edr.exe connector list

# To list all the connectors available in the registry, use find
PS > .\edr.exe connector find

# To install a connector, use add
PS > .\edr.exe connector add Reductech.EDR.Connectors.Sql
```

## Some Examples to Try

Convert JSON To CSV

```
FileRead 'C:/data.json'
| FromJson
| ToCSV
| FileWrite 'C:/data.csv'
```

Remove duplicates from CSV

```
FileRead 'C:/data.csv'
| FromCSV
| ArrayDistinct <>
| ToCSV
| FileWrite 'C:/data-distinct.csv'
```

Remove rows with duplicate Ids from CSV

```
FileRead 'C:/data.csv'
| FromCSV
| ArrayDistinct (From <> 'Id')
| ToCSV
| FileWrite 'C:/data-distinct.csv'
```

## VS Code

A [Visual Studio Code](https://code.visualstudio.com/) extension for
SCL is now available for download from the
[Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=reductech.reductech-scl).

To install in VS Code:

1. Open the Extensions sidebar (`Ctrl + Shift + X`)
2. Search for Reductech
3. Click on SCL and select Install

The extension currently supports:

- Syntax highlighting
- Hover help
- Code completion for step names and parameters
- Error Diagnostics
- Formatting

### Run SCL from VS Code (F5)

To run SCL in VS Code using `F5`, the EDR path needs to be set
in the settings.

1. Go to `File > Preferences > Settings`
2. Navigate to `Extensions > Reductech SCL`
3. Set `EDR: Path`

Or, add the following token to your `settings.json` with a full
path to the EDR executable:

```json
"reductech-scl.edr.path": "D:\\path\\to\\edr.exe",
```
