# EDR Settings

EDR requires an `appsettings.json` that contains the following sections:

- [NLog](#logging) should contain logging configuration

A [default](~/edr/examples/configuration/appsettings-default.json) file is
provided with the application.

Examples can be found [here](~/edr/examples/configuration.md).

## Logging

Core uses structured logging across its Steps and Connectors, and
EDR uses [NLog](https://nlog-project.org) for writing log output to
various targets. NLog is fully configurable using the `appsettings.json`
file.

By default, only output to console and a text file is enabled, but there are other
log targets available including JSON file and Elastic Search.

Please see the [logging documentation](~/edr/how-to/logging.md) for details and configuration examples.

## Connector Configuration

Connectors can be configured using the `connectors.json` file.
However, most configuration should be done by using `edr connector` command.
See the [quick start](~/edr/how-to/quick-start.md#connectors) for more info.

An example connector configuration:

```json
"ConfigurationName": {
  "id": "Connector.Id",
  "enable": true,
  "version": "0.9.0",
  "settings": {
    "additionalConfiguration": "This section is optional"
  }
}
```

### Supported Configuration Keys

| Key                | Description                                                                                                                                      |
| :----------------- | :----------------------------------------------------------------------------------------------------------------------------------------------- |
| Configuration Name | The name of the connector configuration. Must be unique.                                                                                         |
| `id`               | The ID of the connector in the connector registry.                                                                                               |
| `enable`           | If the configuration is enabled. Multiple configurations with the same `id` can exist, but only one of them can be enabled at a time.            |
| `version`          | The version of the connector to install.                                                                                                         |
| `settings`         | An optional settings block. These are connector specific. See [nuix connector](~/edr/connectors/nuix.md#nuix-connector-settings) for an example. |

See the individual [connectors](~/edr/connectors/nuix.md) documentation for more details
on what `settings` are supported.
