# EDR API

All the API docs for Core and the EDR Connectors are here.

These are probably not the docs you're looking for...

Documentation for:

- The Sequence Configuration Language is [here](~/edr/how-to/scl/sequence-configuration-language.md)
- The available Connectors is [here](~/edr/connectors/core.md)
- The available Steps is [here](~/edr/steps/all.md)
