$ErrorActionPreference = 'Stop'
$toc = './edr/steps/toc.yml'
$output = @("- name: All", "  href: all.md")
$output += Get-ChildItem -Path ./edr/steps -Directory | Get-ChildItem -Filter *.md |
    ForEach-Object {
        $content = Get-Content $_.FullName
        [pscustomobject]@{
            Connector = $_.Directory.Name
            Name = ($content[0] -replace '^#+ ').Trim()
            File = $_.Name
        }
    } |
    Group-Object Connector | ForEach-Object {
        if ($_.Name -eq 'Enums') {
            "- name: $($_.Name)`n  items:"
        } else {
            "- name: $($_.Name)`n  href: $($_.Name).md`n  items:"
        }
        $_.Group.ForEach({
            "  - name: $($_.Name)`n    href: $($_.Connector)/$($_.File)"
        })
    }
$output | Out-File $toc -Encoding utf8
