# Reductech E-Discovery Reduct

EDR is a collection of libraries that facilitates the automation
of cross-application e-discovery and forensic workflows.

EDR includes:

- [Core](~/edr/connectors/core.md) which is:
  - An interpreter for the [Sequence Configuration Language](~/edr/how-to/scl/sequence-configuration-language.md)
  - A collection of [application-independent Steps](~/edr/steps/Core.md)
  - An SDK that allows [developers to create Connectors](~/edr/how-to/developers-guide.md)
- Connectors that interact with various applications:
  - [FileSystem](~/edr/connectors/filesystem.md)
  - [Nuix](~/edr/connectors/nuix.md)
  - [PowerShell](~/edr/connectors/powershell.md)
  - [Relativity](~/edr/connectors/relativity.md)
  - [REST](~/edr/connectors/rest.md)
  - [Singer](~/edr/connectors/singer.md)
  - [SQL](~/edr/connectors/sql.md)
  - [StructuredData](~/edr/connectors/structureddata.md)
  - [Tesseract](~/edr/connectors/tesseract.md)
  - [TSK/Autopsy](~/edr/connectors/tsk.md)

## Download and Try

- The EDR command-line application can be downloaded [here](https://gitlab.com/reductech/edr/edr/-/releases)
- A quick start is available [here](~/edr/how-to/quick-start.md)

## Source Code

- [Available on GitLab](https://gitlab.com/reductech/edr)
- [Release Notes](https://gitlab.com/reductech/edr/edr/-/blob/master/CHANGELOG.md)

## Steps and Sequences

- A `Step` is a unit of work in an application such as
  creating a case, ingesting data, searching or exporting data
- A `Sequence` is a series of `Steps` that are executed in order.
  Sequence are defined using SCL and EDR enables data and configuration
  to be passed between Steps, including between different applications.

More about [steps](~/edr/how-to/scl/sequence-configuration-language.md#steps)
and [sequences](~/edr/how-to/scl/sequence-configuration-language.md#sequences).
